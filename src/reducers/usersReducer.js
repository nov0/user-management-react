import {
    USER_DELETE_OPEN,
    USER_DELETE_DONE,
    USER_DELETE_CLOSE,
    USER_LOADING,
    USER_ACTION_ERROR,
    USER_OPEN_EDIT_FORM,
    USER_CLOSE_FORM,
    USER_REFRESH,
    USER_OPEN_FORM_NEW,
} from '../constants/users'

const initialStore = {
    user: {},
    isDeleteOpen: false,
    isFormOpen: false,
    isEditForm: false,
    isLoading: false,
    isError: false,
    isRefreshUsers: false,
    isAboutDialogOpen: false
}

export const usersReducer = (store = initialStore, action) => {
    switch (action.type) {
        case USER_DELETE_OPEN:
            return Object.assign({}, store, {
                user: action.payload,
                isDeleteOpen: true
            })
        case USER_LOADING:
            return Object.assign({}, store, {
                isLoading: true,
                isError: false
            })
        case USER_DELETE_CLOSE:
            return Object.assign({}, store, {
                user: Object.assign({}),
                isDeleteOpen: false,
                isError: false,
                isLoading: false
            })
        case USER_DELETE_DONE:
            return Object.assign({}, store, {
                user: Object.assign({}),
                isDeleteOpen: false,
                isLoading: false,
                isError: false
            })
        case USER_ACTION_ERROR:
            return Object.assign({}, store, {
                isLoading: false,
                isError: true
            })
        case USER_OPEN_FORM_NEW:
            return Object.assign({}, store, {
                user: Object.assign({}),
                isFormOpen: true,
                isLoading: false
            })
        case USER_OPEN_EDIT_FORM:
            return Object.assign({}, store, {
                user: Object.assign({}, action.payload),
                isFormOpen: true,
                isEditForm: true,
                isLoading: false,
                isError: false
            })
        case USER_CLOSE_FORM:
            return Object.assign({}, store, {
                user: Object.assign({}),
                isFormOpen: false,
                isDeleteOpen: false,
                isEditForm: false,
                isLoading: false,
                isError: false
            })
        case USER_REFRESH:
            return Object.assign({}, store, {
                isRefreshUsers: !store.isRefreshUsers
            })
        default:
            return store;
    }
}
