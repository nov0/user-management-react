import { SHOW_USER_MESSAGE, HIDE_USER_MESSAGE } from '../constants/messages'

const initialStore = {
    isShowMessage: false,
    type: '',
    text: ''
}

export const messagesReducer = (store = initialStore, action) => {
    switch (action.type) {
        case SHOW_USER_MESSAGE:
            return Object.assign({}, store, {
                isShowMessage: true,
                type: action.payload.type,
                text: action.payload.text
            })
        case HIDE_USER_MESSAGE:
            return Object.assign({}, store, {
                isShowMessage: false,
                type: '',
                text: ''
            })
        default:
            return store;
    }
}
