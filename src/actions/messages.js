import { SHOW_USER_MESSAGE, HIDE_USER_MESSAGE } from "../constants/messages";

export const showMessage = payload => (
    { type: SHOW_USER_MESSAGE, payload }
)

export const hideMessage = () => (
    { type: HIDE_USER_MESSAGE }
)
