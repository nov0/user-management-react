import {
    USER_DELETE_OPEN,
    USER_DELETE_CLOSE,
    USER_DELETE_DONE,
    USER_LOADING,
    USER_ACTION_ERROR,
    USER_OPEN_EDIT_FORM,
    USER_CLOSE_FORM,
    USER_REFRESH,
    USER_OPEN_FORM_NEW
} from '../constants/users'
import { USER_ENDPOINT, EDIT_USER_ENDPOINT } from '../constants/endpoints'

import { showMessage } from './messages'
import axios from 'axios'

export const openEditUserForm = elasticUser => dispatch => {
    const userId = elasticUser.userId
    if (elasticUser.id && userId) {
        dispatch(userLoading())
        axios.get(USER_ENDPOINT + userId)
            .then(response => {
                const user = response.data
                user.elasticId = elasticUser.id
                dispatch(openUserForm(user))
            })
            .catch(err => {
                dispatch(userActionError())
                dispatch(showMessage({
                    type: 'error',
                    text: 'Error while fetching user'
                }))
            })
    } else {
        dispatch(showMessage({
            type: 'error',
            text: 'Something went wrong. Can\'t get user'
        }))
    }
}

export const deleteUser = () => (dispatch, getStore) => {
    const store = getStore()
    const userId = store.users.user.id
    dispatch(userLoading())
    axios.delete(USER_ENDPOINT + userId)
        .then(response => {
            dispatch(userDeleted())
            dispatch(refreshUsers())
            dispatch(showMessage({
                type: 'success',
                text: 'User deleted successfully'
            }))
        })
        .catch(err => {
            dispatch(userActionError())
            dispatch(showMessage({
                type: 'error',
                text: 'Error while deleting user'
            }))
        })
}

const newUser = (user, dispatch) => {
    dispatch(userLoading())
    axios.post(USER_ENDPOINT, user)
        .then(response => {
            dispatch(closeUserDialog())
            dispatch(refreshUsers())
            dispatch(showMessage({
                type: 'success',
                text: 'New user added successfully'
            }))
        })
        .catch(err => {
            dispatch(userActionError())
            dispatch(showMessage({
                type: 'error',
                text: 'Error while adding new user'
            }))
        })
}

const handleRequest = (success, error) => {

}

const editUser = (user, dispatch) => {
    dispatch(userLoading())
    axios.put(EDIT_USER_ENDPOINT + user.elasticId, user)
        .then(handleRequest)
        .then(response => {
            dispatch(closeUserDialog())
            dispatch(refreshUsers())
            dispatch(showMessage({
                type: 'success',
                text: 'User edited successfully'
            }))
        })
        .catch(err => {
            dispatch(userActionError())
            dispatch(showMessage({
                type: 'error',
                text: 'Error while eidting user'
            }))
        })
}

export const saveUser = user => dispatch => {
    if (user.id) {
        editUser(user, dispatch)
    } else {
        newUser(user, dispatch)
    }
}

export const openUserDelete = payload => (
    { type: USER_DELETE_OPEN, payload }
)

export const userDeleted = () => (
    { type: USER_DELETE_DONE }
)

const userLoading = () => (
    { type: USER_LOADING }
)

const userActionError = () => (
    { type: USER_ACTION_ERROR }
)

export const closeUserDeleteDialog = () => (
    { type: USER_DELETE_CLOSE }
)

export const openUserForm = payload => (
    { type: USER_OPEN_EDIT_FORM, payload }
)

export const closeUserDialog = () => (
    { type: USER_CLOSE_FORM }
)

export const refreshUsers = () => (
    { type: USER_REFRESH }
)

export const openUserFormNew = () => (
    { type: USER_OPEN_FORM_NEW }
)
