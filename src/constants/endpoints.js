 export const HOST = 'http://ec2-18-184-32-156.eu-central-1.compute.amazonaws.com'
// const HOST = "http://localhost";
const API_PORT = ":8088"
const OAUTH_PORT = ":5389"
export const USER_ENDPOINT = HOST + API_PORT + '/user/'
export const EDIT_USER_ENDPOINT = USER_ENDPOINT + "?id="
export const USER_SEARCH = USER_ENDPOINT + 'search/'

export const TOKEN_ENDPOINT = HOST + OAUTH_PORT + "/oauth/token"
