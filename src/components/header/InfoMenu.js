import React, { useState } from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import AboutDialog from '../AboutDialog'
import authService from '../../utils/auth-service'

const InfoMenu = ({ anchorEl, setAnchorEl }) => {

    const [isAboutDialogOpen, setAboutDialogOpen] = useState(false)

    const menuId = 'primary-search-account-menu'
    const isMenuOpen = Boolean(anchorEl)

    const handleMenuClose = () => {
        setAnchorEl(null)
        setAboutDialogOpen(true)
    }
    
    const handleLogout = () => {
        authService.removeAuthentication()
        setAnchorEl(null)
        window.location.href = '#/login';
    }

    return (
        <div>
            <Menu
                anchorEl={anchorEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                id={menuId}
                keepMounted
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMenuOpen}
                onClose={handleMenuClose}>
                <MenuItem onClick={handleMenuClose}>Info</MenuItem>
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
            </Menu>
            {!!isAboutDialogOpen &&
                <AboutDialog
                    isAboutDialogOpen={isAboutDialogOpen}
                    closeAboutDialog={setAboutDialogOpen}
                />}
        </div>
    )
}

export default InfoMenu
