import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton';
import InfoRounded from '@material-ui/icons/InfoRounded';
import InfoMenu from './InfoMenu';
import Navigation from './Navigation';
import authService from '../../utils/auth-service'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing(1),
    color: 'wihte'
  },
}));

const Header = () => {

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const menuId = 'primary-search-account-menu';

  function handleProfileMenuOpen(event) {
    setAnchorEl(event.currentTarget);
  }

  const isAuthenticated = authService.isAuthenticated()

  return (
    isAuthenticated && <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Navigation />
          <Typography variant="h6" className={classes.title}>
            User management
              </Typography>
          {/* <Button color="inherit">Login</Button> */}
          <IconButton
            edge="end"
            aria-label="Account of current user"
            aria-controls={menuId}
            aria-haspopup="true"
            onClick={handleProfileMenuOpen}
            color="inherit">
            <InfoRounded />
          </IconButton>
        </Toolbar>
      </AppBar>
      <InfoMenu anchorEl={anchorEl} setAnchorEl={setAnchorEl} />
    </div>
  );
}

export default Header