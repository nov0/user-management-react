import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTheme } from '@material-ui/core/styles';
import Slide from '@material-ui/core/Slide';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DateFnsUtils from '@date-io/date-fns';
import CircularProgress from '@material-ui/core/CircularProgress'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';

// create modal open transition effect
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const initialUser = {
    firstName: '',
    lastName: '',
    address: '',
    email: '',
    password: '',
    phoneNumber: '',
    imageUrl: '',
    birthDate: new Date()
}

const UserFormDialog = ({ user, isFormOpen, userId, isEditForm, isLoading, closeUserDialog, saveUser }) => {

    const [userDetails, setUserDetails] = useState(Object.assign({}, initialUser, user))
    const [showPassword, setShowPassword] = useState(false)

    const handleChange = event => {
        setUserDetails({ ...userDetails, [event.target.name]: event.target.value });
    };

    const handleBirthDateChange = birthDate => {
        setUserDetails({ ...userDetails, birthDate: birthDate });
    }

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword)
    }

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <Dialog
            open={isFormOpen}
            onClose={closeUserDialog}
            TransitionComponent={Transition}
            fullScreen={fullScreen}
            maxWidth={'sm'}
            fullWidth={true}
            aria-labelledby="form-dialog-title">

            <DialogTitle id="form-dialog-title">
                {isEditForm ? 'Edit' : 'Add new'} user
                    {!!fullScreen &&
                    <IconButton color="inherit" onClick={closeUserDialog} aria-label="Close" style={{ float: "right", margin: '-10px 0 0 0' }}>
                        <CloseIcon />
                    </IconButton>
                }
            </DialogTitle>
            <ValidatorForm onSubmit={() => saveUser(userDetails, userId)}>
                <DialogContent>
                    <TextValidator
                        autoFocus
                        id="firstName"
                        name="firstName"
                        value={userDetails.firstName}
                        onChange={handleChange}
                        margin="dense"
                        label="First Name *"
                        type="text"
                        validators={['required']}
                        errorMessages={['Please input first name']}
                        fullWidth />
                    <TextValidator
                        id="lastName"
                        name="lastName"
                        value={userDetails.lastName}
                        onChange={handleChange}
                        margin="dense"
                        label="Last Name *"
                        type="text"
                        validators={['required']}
                        errorMessages={['Please input last name']}
                        fullWidth />
                    <TextValidator
                        id="address"
                        name="address"
                        value={userDetails.address}
                        onChange={handleChange}
                        margin="dense"
                        label="Address *"
                        type="text"
                        validators={['required']}
                        errorMessages={['Please input address']}
                        fullWidth />
                    <TextValidator
                        id="email"
                        name="email"
                        value={userDetails.email}
                        onChange={handleChange}
                        margin="dense"
                        label="e-mail *"
                        type="text"
                        validators={['required', 'isEmail']}
                        errorMessages={['Please input address', 'e-mail is not valid']}
                        fullWidth />
                    <TextValidator
                        id="password"
                        name="password"
                        value={userDetails.password}
                        onChange={handleChange}
                        margin="dense"
                        label="Password *"
                        type={showPassword ? 'text' : 'password'}
                        validators={['required', 'minStringLength:8']}
                        errorMessages={['Please input password', 'Password must contain at least 8 characters']}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">
                            <IconButton aria-label="Toggle password visibility" onClick={handleClickShowPassword}>
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                          }}
                        fullWidth />
                    <TextValidator
                        id="phoneNumber"
                        name="phoneNumber"
                        value={userDetails.phoneNumber}
                        onChange={handleChange}
                        margin="dense"
                        label="Phone number *"
                        type="text"
                        validators={['required']}
                        errorMessages={['Please input phone']}
                        fullWidth
                        />
                    <TextValidator
                        id="imageUrl"
                        name="imageUrl"
                        value={userDetails.imageUrl}
                        onChange={handleChange}
                        margin="dense"
                        label="Image URL"
                        type="text"
                        fullWidth
                        />
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            id="birthDate"
                            name="birthDate"
                            label="Date of birth"
                            value={userDetails.birthDate}
                            onChange={handleBirthDateChange}
                            margin="dense"
                            fullWidth
                            KeyboardButtonProps={{
                                'aria-label': 'change date'
                            }}
                        />
                    </MuiPickersUtilsProvider>
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeUserDialog} color="primary">Cancel</Button>
                    <Button type="submit" disabled={isLoading} color="primary">
                        {isLoading ? <CircularProgress size={20} /> : (isEditForm ? 'Edit' : 'Save')}
                    </Button>
                </DialogActions>
            </ValidatorForm>
        </Dialog>
    )
}

export default UserFormDialog
