import React from 'react'

function NotFound() {
    return (
        <div> 
            Desired page is not found!!!
        </div>
    )
}

export default NotFound
