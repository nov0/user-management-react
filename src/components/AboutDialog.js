import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const AboutDialog = ({ isAboutDialogOpen, closeAboutDialog }) => (
    <Dialog open={isAboutDialogOpen} onClose={closeAboutDialog} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">About</DialogTitle>
        <DialogContent>
            <DialogContentText>
                This is test project with React!
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={() => closeAboutDialog(false)} color="primary">Close</Button>
        </DialogActions>
    </Dialog>
)

export default AboutDialog
