import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress'

const UserDeleteDialog = ({ isDeleteOpen, closeUserDeleteDialog, deleteUser, isLoading }) => (
    <Dialog open={isDeleteOpen} onClose={closeUserDeleteDialog} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Delete user</DialogTitle>
        <DialogContent>
            <DialogContentText>
                Are you sure that you wanna delete this user?
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={closeUserDeleteDialog} color="primary">Cancel</Button>
            <Button onClick={deleteUser} disabled={isLoading} color="primary">
                {isLoading ? <CircularProgress size={20} /> : 'Delete'}
            </Button>
        </DialogActions>
    </Dialog>
)

export default UserDeleteDialog
