import React, { useEffect } from 'react'
import MaterialTable from "material-table";
import IconButton from '@material-ui/core/IconButton';
import DeleteRounded from '@material-ui/icons/DeleteRounded';
import EditRounded from '@material-ui/icons/EditRounded';
import UserDeleteDialogContainer from '../containers/UserDeleteDialogContainer';
import Moment from 'react-moment';
import axios from '../utils/api'
import UserFormDialogContainer from '../containers/UserFormDialogContainer';
import { USER_SEARCH } from '../constants/endpoints'

const Users = ({ openUserDelete, openEditUserForm, openNewUserForm, isFormOpen, isDeleteOpen, isRefreshUsers, isLoading }) => {

    useEffect(() => {
        tableRef.current.onQueryChange();
    }, [isRefreshUsers])

    const hideColumn = () => (
        window.innerWidth <= 600
    )



    const columnDefinitions = [
        { title: "Name", field: "firstName", },
        { title: "Lastname", field: "lastName" },
        { title: "Birthdate", field: "birthDate", hidden: hideColumn(), render: rowData => <Moment format="MM/DD/YYYY" date={rowData.birthDate} /> },
        { title: "Image", field: "imageUrl", sorting: false, hidden: hideColumn(), render: rowData => <img alt="User avatar" src={rowData.imageUrl} width="32" /> },
        {
            title: "Action", field: "id", sorting: false, render: rowData =>
                <span>
                    <IconButton edge="start" color="inherit" aria-label="Menu" onClick={() => openEditUserForm(rowData)} disabled={isLoading}>
                        <EditRounded />
                    </IconButton>
                    <IconButton edge="start" color="inherit" aria-label="Menu" onClick={() => openUserDelete(rowData)} disabled={isLoading}>
                        <DeleteRounded />
                    </IconButton>
                </span>
        },
    ]

    const tableRef = React.createRef();

    const refresh = () => {
        tableRef.current.onQueryChange()
    }

    return (
        <div style={{ maxWidth: "100%" }}>
            <MaterialTable
                title="User table"
                columns={columnDefinitions}
                options={{ pageSize: 20, pageSizeOptions: [10, 20, 50, 100] }}
                data={query =>
                    new Promise((resolve, reject) => {
                        const params = {
                            columnName: query && query.orderBy ? query.orderBy.field : 'firstName',
                            order: query.orderDirection || '',
                            query: query.search || '',
                            pageIndex: query.page || 0,
                            pageSize: query.pageSize || 0,
                        }
                        axios.post(USER_SEARCH, params)
                            .then(result => {
                                resolve({
                                    data: result.data.users,
                                    page: query.page,
                                    totalCount: result.data.total,
                                })
                            })
                    })
                }
                tableRef={tableRef}
                actions={[
                    {
                        icon: 'refresh',
                        tooltip: 'Refresh table',
                        isFreeAction: true,
                        onClick: refresh
                    },
                    {
                        icon: 'add',
                        tooltip: 'Add new user',
                        isFreeAction: true,
                        onClick: event => openNewUserForm()
                    }
                ]}
            />
            {!!isFormOpen && <UserFormDialogContainer />}
            {!!isDeleteOpen && <UserDeleteDialogContainer />}
        </div>
    )
}

export default Users
