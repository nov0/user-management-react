import React from 'react'
import usersImage from '../assets/img/user-group-icon-10.jpg'

const Home = () => {
    return (
        <div className="center-text">
            <h1>Welcome to user management</h1>
            <img className="padding-top-20 image-animation"
                src={usersImage}
                alt="Users"/>
        </div>
    )
}

export default Home
