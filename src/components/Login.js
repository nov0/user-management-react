import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import { TOKEN_ENDPOINT } from '../constants/endpoints'
import { CLIENT_CREDENTIALS } from '../constants/credentials'
import axios from 'axios'
import qs from 'qs'
import authService from '../utils/auth-service'

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const Login = () => {

    const [loginData, setLoginData] = useState(Object.assign({}, {username: "", password: ""}))
    const [loginError, setLoginError] = useState(false)
    const [showPassword, setShowPassword] = useState(false)
    const classes = useStyles();

    const handleChange = event => {
        setLoginData({ ...loginData, [event.target.name]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword)
    }

    const login = () => {
        const config = {
            headers: {
                "Authorization" : "Basic " + CLIENT_CREDENTIALS,
                "Content-Type" : "application/x-www-form-urlencoded"
            }
        }
        const data = qs.stringify({ // query string parser
            username: loginData.username,
            password: loginData.password,
            grant_type: "password"
        })

        // reset login error
        setLoginError(false)

        axios.post(TOKEN_ENDPOINT, data, config)
            .then(response => {
                authService.saveAuthentication(response.data)
                window.location.href = '/';
            })
            .catch(err => {
                setLoginData({...loginData, password: ''})
                setLoginError(true)
            })
    }

    return (
        <Container component="main" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <ValidatorForm onSubmit={() => login()}>
                    <TextValidator
                        autoFocus
                        id="username"
                        name="username"
                        value={loginData.username}
                        onChange={handleChange}
                        variant="outlined"
                        margin="normal"
                        label="Username"
                        type="text"
                        autoComplete="username"
                        validators={['required']}
                        errorMessages={['']}
                        fullWidth />
                    <TextValidator
                        id="password"
                        name="password"
                        value={loginData.password}
                        onChange={handleChange}
                        variant="outlined"
                        margin="normal"
                        label="Password"
                        type={showPassword ? 'text' : 'password'}
                        validators={['required']}
                        errorMessages={['']}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">
                            <IconButton aria-label="Toggle password visibility" onClick={handleClickShowPassword}>
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                        }}
                        fullWidth />
                    {
                        !!loginError && <Typography component="div" color="error">
                        Bad credentials. Please try again.</Typography>
                    }
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}>
                            Sign in
                    </Button>

                    <Grid container>
                        <Grid item xs>
                            <Link href="#" variant="body2">Forgot password?</Link>
                        </Grid>
                        <Grid item>
                            <Link href="#" variant="body2">{"Don't have an account? Sign Up"}</Link>
                        </Grid>
                    </Grid>
                </ValidatorForm>
            </div>
        </Container>
    );
}

export default Login