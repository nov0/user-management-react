const AUTH_KEY = 'authentication';

const removeAuthentication = () => localStorage.removeItem(AUTH_KEY)
const isAuthenticated = () => !!localStorage.getItem(AUTH_KEY)
const getAuthentication = () => JSON.parse(localStorage.getItem(AUTH_KEY))
const saveAuthentication  = auth => localStorage.setItem(AUTH_KEY, JSON.stringify(auth))

const authService = {
    removeAuthentication,
    isAuthenticated,
    saveAuthentication,
    getAuthentication
}

export default authService
