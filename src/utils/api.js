import axios from "axios";
import authService from './auth-service'

axios.interceptors.request.use(
    config => {
        const authentication = authService.getAuthentication()
        if (authentication && config.url.indexOf("/oauth/token") === -1) {
            config.headers.Authorization = "Bearer " + authentication.access_token
        }
        return config;
    },
    error => Promise.reject(error)
);

axios.interceptors.response.use(
    response => response,
    error => {
        if (error.response && error.response.status === 401) { // unauthorized request, removign authenitcation object and redirect to /login
            authService.removeAuthentication()
            window.location.href = '/login'
        }
        return Promise.reject(error);
    }
);

export default axios