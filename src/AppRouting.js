import React from 'react'
import { HashRouter, Switch } from 'react-router-dom'

// components
import Home from './components/Home';
import NotFound from './components/NotFound';
import Header from './components/header/Header';
import UsersContainer from './containers/UsersContainer';
import Login from './components/Login';

import RouteContainer from './containers/RouteContainer';
import RouteLogin from './containers/RouteLogin';

const AppRouting = () => {
    return (
        <HashRouter>
            <Header></Header>
            <Switch>
                <RouteContainer exact path="/" component={Home} />
                <RouteContainer path="/users" component={UsersContainer} />
                <RouteLogin exact path="/login" component={Login} />
                <RouteContainer component={NotFound} />
            </Switch>
        </HashRouter> 
    )
}

export default AppRouting
