import { connect } from 'react-redux';
import Users from '../components/Users';
import {
    openUserDelete,
    openEditUserForm,
    openUserFormNew
} from '../actions/users'

const mapStateToProps = state => ({
    isLoading: state.users.isLoading,
    isFormOpen: state.users.isFormOpen,
    isDeleteOpen: state.users.isDeleteOpen,
    isRefreshUsers: state.users.isRefreshUsers
})

const mapDispatchToProps = dispatch => ({
    openUserDelete: user => dispatch(openUserDelete(user)),
    openEditUserForm: payload => dispatch(openEditUserForm(payload)),
    openNewUserForm: () => dispatch(openUserFormNew())
})

const UsersContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Users)

export default UsersContainer