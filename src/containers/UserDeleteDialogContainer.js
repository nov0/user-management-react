import { connect } from 'react-redux';
import UserDeleteDialog from '../components/UserDeleteDialog';
import { closeUserDeleteDialog, deleteUser } from '../actions/users'

const mapStateToProps = state => ({
    user: state.users.user,
    isDeleteOpen: state.users.isDeleteOpen,
    isLoading: state.users.isLoading
})

const mapDispatchToProps = payload => dispatch => (
    { 
        closeUserDeleteDialog: () => dispatch(closeUserDeleteDialog()),
        deleteUser: () => dispatch(deleteUser())
    }
)

const UserDeleteDialogContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserDeleteDialog)

export default UserDeleteDialogContainer
