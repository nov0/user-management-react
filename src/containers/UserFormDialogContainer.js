import { connect } from 'react-redux';
import { closeUserDialog, saveUser } from '../actions/users'
import UserFormDialog from '../components/UserFormDialog';

const mapStateToProps = state => ({
    user: state.users.user,
    isFormOpen: state.users.isFormOpen,
    isEditForm: state.users.isEditForm,
    isLoading: state.users.isLoading
})

const mapDispatchToProps = dispatch => (
    {
        closeUserDialog: () => dispatch(closeUserDialog()),
        saveUser: user => dispatch(saveUser(user)),
    }
)

const UserFormDialogContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserFormDialog)

export default UserFormDialogContainer