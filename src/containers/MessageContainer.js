import { connect } from 'react-redux';
import { hideMessage } from '../actions/messages'
import Messages from '../components/Messages';

const mapStateToProps = state => ({
    isShowMessage: state.messages.isShowMessage,
    type: state.messages.type,
    text: state.messages.text
})

const mapDispatchToProps = dispatch => (
    {
        hideMessage: () => dispatch(hideMessage()),
    }
)

const MessagesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Messages)

export default MessagesContainer