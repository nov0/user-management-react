import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import authService from '../utils/auth-service'

const RouteLogin = ({component: Component, ...allProps}) => (
    <Route {...allProps} render={props => (
        !authService.isAuthenticated() ?
            <Component {...props}/> :
            <Redirect to={{ pathname: '/', state: { from: props.location } }} />
    )} />
)

export default RouteLogin
