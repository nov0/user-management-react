import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import authService from '../utils/auth-service'

const RouteContainer = ({component: Component, ...allProps}) => (
    <Route {...allProps} render={props => (
        authService.isAuthenticated() ?
            <Component {...props}/> :
            <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)

export default RouteContainer
